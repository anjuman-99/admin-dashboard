import { Box, AppBar, Toolbar, Typography, styled, Menu, MenuItem, Button, Fade, InputBase } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import SearchIcon from '@mui/icons-material/Search';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import MoreVertOutlinedIcon from '@mui/icons-material/MoreVertOutlined';
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import URL from '../../Assets/cloud.jpg'
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';


const StyledNavbar = styled(AppBar)`
   color : #FFFFFF;
   background-color : #06139E;
`
const StyledText = styled(Typography)`
   color : #FFFFFF;
   font-weight: 500;
   font-size: 18px;
   margin-left: 15px;
   cursor : pointer;
   font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
`;

const StyledTabs = styled(Box)`
  margin-left: 15px;
  display: flex;
`;

const Add = styled(Box)`
   background-color: #FFFFFF;
   width: 45px;
   justify-content: center;
   align-items: center;
   text-align: center;
   margin-left: 250px;
`;

const SearchContainer = styled(Box)`
     background : #fff;
     width : 17.5%;
     border-radius : 2px;
     margin-left : 10px;
     display : flex;
`;

const InputSearchBase = styled(InputBase)`
      padding-left : 10px;
      width : 100%;
      font-size : unset; 
`;

const StyledSearch = styled(Box)`
    color :  blue;
    padding : 5px;
    display : flex;
`;

const Gift = styled(Box)`
background-color: #c4381b;
text-align: center;
width: 40px;
height: 40px;
margin-left: 10px;
`;


const Image = styled('img')({
    border: '1px solid black'
})

const Navbar = () => {

    const navigate = useNavigate();

    const [anchorEl, setAnchorEl] = useState(null);

    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <Box>
            <StyledNavbar postion='static'>
                <Toolbar style={{ minHeight: '57px' }}>

                    <Image src={URL} alt='img' width='200px' height='42px' />

                    <StyledTabs>
                        <StyledText onClick={() => navigate('/')}>Servers</StyledText>
                        <StyledText onClick={() => navigate('/app')}>Application</StyledText>
                        <StyledText onClick={() => navigate('/team')}>Team</StyledText>
                        <StyledText onClick={() => navigate('/projects')}>Projects</StyledText>
                        <StyledText onClick={() => navigate('/affiliate')}>Affiliate</StyledText>
                    </StyledTabs>

                    <Button
                        id="fade-button"
                        aria-controls={open ? 'fade-menu' : undefined}
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        onClick={handleClick}
                    >
                        <MenuIcon style={{ paddingLeft: '10px', fontSize: '35px', color: '#FFFFFF' }} />
                    </Button>
                    <Menu
                        id="fade-menu"
                        MenuListProps={{
                            'aria-labelledby': 'fade-button',
                        }}
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        TransitionComponent={Fade}
                    >
                        <MenuItem onClick={() => {
                            setAnchorEl(null);
                        }}>Option 1</MenuItem>
                        <MenuItem onClick={() => {
                            setAnchorEl(null);
                        }}>Option 2</MenuItem>
                    </Menu>
                    <Add>
                        <AddRoundedIcon style={{ color: 'black', height: '57px', }} />
                    </Add>

                    <SearchContainer style={{ display: 'flex' }}>
                        <InputSearchBase
                            placeholder="Search Server or Application"
                        />
                        <StyledSearch>
                            <SearchIcon />
                        </StyledSearch>
                    </SearchContainer>
                    <AccountCircleOutlinedIcon style={{ marginLeft: '10px', fontSize: '35px' }} />
                    <MoreVertOutlinedIcon style={{ marginLeft: '5px', fontSize: '30px' }} />
                    <Gift>
                        <CardGiftcardIcon style={{ color: 'FFFFFF', fontSize: '25px', marginTop: '7px' }} />
                    </Gift>
                </Toolbar>
            </StyledNavbar>
        </Box>


    )
}

export default Navbar;

