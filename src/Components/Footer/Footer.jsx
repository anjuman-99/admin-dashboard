import React from 'react'
import './Footer.css'

const Footer = () => {
  return (
   <div className="footer">
    <div className="footercontainer">
     
     <div className='footercontainer_div_first'>
        <div style={{width:'100%', paddingTop:5, marginRight:4, marginLeft:5,}}><p style={{fontSize:'14px', fontWeight:'bold'}}>YOU'RE ON FREE TRIAL</p></div>
        <div style={{width:'100%', paddingTop:5, marginRight:4,marginLeft:5,}}><p style={{fontSize:14,}}>Billing start once you upgrade</p></div>
     </div>
     <div className='footercontainer_div_line' >
        <div style={{width:1, backgroundColor:'#00000029', height:55}}></div>
     </div>
     <div className='footercontainer_div_Hourly'>
        <div style={{fontSize:12,}}><p>Hourly</p>
        <h5>$0.0361</h5></div>
     </div>
     <div className='footercontainer_div_monthly'>
     <div><p style={{fontSize:12,}}>Monthly</p>
        <h5>$26.00</h5></div>
     </div>   
     <div className='footercontainer_div_text'>
     <div><p style={{fontSize:15, marginTop:14,}}>To Lounch free trial you need to verify your account first.</p></div>
     </div>   
     <div className='footercontainer_div_upgrade'>
        <button >LAUNCH NOW</button></div>    
    
    </div>
   </div>
  )
}

export default Footer