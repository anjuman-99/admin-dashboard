import React from 'react'
import  './server.css'
import digitalocean from '../../Assets/img/images.png'
import  vultr from '../../Assets/img/16991178281552037063-128.png'
import linode from '../../Assets/img/Linode-Logo.wine.png'
import aws from '../../Assets/img/AWS-logo-2.jpg'
import google from '../../Assets/img/GoogleLogo.png'


const Server = () => {
  return (
    <div className='container'>
       <div className='headingtext'>
        <p className='headingtext_text'>DEPLOY YOUR MANAGED APPLICATION</p>
       </div>
       <div className='cont'>
        <div className='cont_container'>
          <div>
              <p style={{marginTop:60}}>APPLICATION & SERVER DETAILS</p>
          </div> 

         <div className='apliicationd_etails_cont'>
            <div className='apliicationd_etails_cont_one'> 
            <p>Wordpress</p>
            <p>Version 6.0</p>
            </div>
            <div className='apliicationd_etails_cont_one'>
            <p>Name Your Managed App</p>
            <p>Your Appilication</p>
            </div>
            <div className='apliicationd_etails_cont_one'>
            <p>Name Your Managed App</p>
            <p>Your Server</p>
            </div>
            <div className='apliicationd_etails_cont_one'>
            <p></p>
            <p className='select_project'>Select Your Project</p>
            </div>
        </div> 

        <div className='img_div_cont'>
        <div className='img_div'>
          <img src={digitalocean} alt="" />
        </div>
        <div className='img_div'>
          <img src={vultr} alt="" />
        </div>
        <div className='img_div'>
          <img src={linode} alt="" />
        </div>
        <div className='img_div'>
          <img src={aws} alt="" />
        </div> 
        <div className='img_div'>
          <img src={google} alt="" />
        </div>  
        </div>
      

     <div className='server_container'>

     <div>
        <p>SERVER SIZE</p>
      </div>
      <div>
        <p>
        Please select your server size. You can always scale your server size whenever required.
        </p>
      </div>

      <div className="slider">
        
      </div>
      
     </div>
      
      
     <div className="location_container">
      <div>
        <p>LOCATION</p>
      </div>
      <div>
        <p>Please select your server location.</p>
      </div>

      <div>
        
      </div>
      </div> 
      
      
      
      
      
      
      </div>
      </div>
    </div>
  )
}

export default Server