import React from 'react';
import Navbar from './Components/Navbar/Navbar';
import Applications from './Components/Applications/Applications'
import Projects from './Components/Projects/Projects'
import Team from './Components/Team/Team'
import Affiliate from './Components/Affiliate/Affiliate'
import { Routes, Route } from 'react-router-dom';
import Server from './Components/Servers/Server';
import Footer from './Components/Footer/Footer'

function App() {
  return (
    <div>
      <Navbar />
      <div style={{ marginTop: '65px' }}>
        <Routes>
          <Route exact path='/' element={<Server />} />
          <Route exact path='/app' element={<Applications />} />
          <Route exact path='/projects' element={<Projects />} />
          <Route exact path='/team' element={<Team />} />
          <Route exact path='/affiliate' element={<Affiliate />} />
        </Routes>
      </div>
      <Footer />
    </div>
  );
}

export default App;
